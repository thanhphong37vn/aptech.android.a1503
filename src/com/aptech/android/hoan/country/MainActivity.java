package com.aptech.android.hoan.country;

import java.util.Scanner;

import static com.aptech.android.hoan.country.Print.println;

/**
 * <h1>System City Management</h1> <br/>
 * <br/>
 * The System City Management program implements an application that<br/>
 * 
 * 1. Input information for n countries of Asia<br/>
 * 2. Display information<br/>
 * 3. Sort descending by area and display result<br/>
 * 4. Sort ascending by area and display result<br/>
 * 5. Find information by countryName<br/>
 * 6. Exit<br/>
 * 
 * 
 * @author Admin
 * @version v.05.30
 * @since 2015-05-30
 * 
 */

public class MainActivity {
	/**
	 * Display menu list and return your choose
	 * 
	 * @return int : chose of user from screen
	 */
	public int doMenu() {

		/** Print menu on standard output */
		println("==========Menu==============");
		println("1.	Input information for n countries of Asia");
		println("2.	Display information");
		println("3.	Sort descending by area and display result");
		println("4.	Sort ascending by area and display result");
		println("5.	Find information by countryName");
		println("6.	Exit");
		// get chose from console
		return getNumberInput();

	}

	/**
	 * This method use to get number value input from console
	 * 
	 * @return number input
	 */
	public int getNumberInput() {
		Scanner scanner = new Scanner(System.in);
		// scanner.useDelimiter("\n");
		println("Nhap vao lua chon cua ban : ");
		while (!scanner.hasNextInt())
			scanner.next();
		return scanner.nextInt();
	}

	/**
	 * This method use to get string value input from console
	 * 
	 * @return string value
	 */
	@SuppressWarnings("resource")
	public String getStringInput() {
		Scanner scanner = new Scanner(System.in);
		println("Nhap vao lua chon cua ban : ");
		return scanner.nextLine();
	}

	/**
	 * This is method main which makes use of MainActivity.
	 * 
	 * @return nothing
	 * 
	 * @param args
	 *            Unused
	 * @exception No
	 *                Exception
	 * @see Exeption
	 * 
	 */
	public static void main(String[] args) {
		MainActivity activity = new MainActivity();
		AsiaCountry[] arrACs = null;
		int chose = 0;

		do {
			chose = activity.doMenu();
			switch (chose) {
			case 1:
				// arrACs = activity.doInput(activity.getNumberInput());
				arrACs = activity.fakeData(activity.getNumberInput());
				break;
			case 2:
				activity.doDisplay(arrACs);
				break;
			case 3:

				activity.doSortAsc(arrACs);
				break;
			case 4:
				activity.doSortDes(arrACs);
				break;
			case 5:

				activity.doSearch(arrACs, activity.getStringInput());
				break;
			case 6:
				println("System exited");
				System.exit(0);
				break;

			default:
				activity.doMenu();
				break;
			}
		} while (chose > 0 || chose < 5);

	}

	/**
	 * 5. Find information by countryName
	 * 
	 * @param arrACs
	 *            AsiaCountry[]
	 * @param byCountryName
	 *            search by this string
	 */
	private void doSearch(AsiaCountry[] arrACs, String byCountryName) {
		if (arrACs != null)
			for (int i = 0; i < arrACs.length; i++)
				if (arrACs[i].getCoutryName().contains(byCountryName))
					arrACs[i].display();
	}

	/**
	 * 4. Sort ascending by area and display result
	 * 
	 * @param arrAsiaCountries
	 */
	private void doSortDes(AsiaCountry[] arrAC) {

		if (arrAC != null) {
			// Sort ascending
			for (int i = 0; i < arrAC.length; i++) {
				for (int j = 1; j < arrAC.length - i; j++) {
					if (arrAC[j].getArea() > arrAC[j - 1].getArea()) {
						//
						AsiaCountry temp = arrAC[j];
						arrAC[j] = arrAC[j - 1];
						arrAC[j - 1] = temp;
					}
				}
			}
			// call method to display result
			doDisplay(arrAC);
		}
	}

	/**
	 * 3. Sort descending by area and display result
	 * 
	 * @param arrAsiaCountries
	 */
	private void doSortAsc(AsiaCountry[] arrAC) {
		// TODO Auto-generated method stub
		if (arrAC != null) {

			for (int i = 0; i < arrAC.length; i++) {
				for (int j = 1; j < arrAC.length - i; j++) {
					if (arrAC[j].getArea() < arrAC[j - 1].getArea()) {
						AsiaCountry temp = arrAC[j];
						arrAC[j] = arrAC[j - 1];
						arrAC[j - 1] = temp;
					}
				}
			}
			doDisplay(arrAC);
		}
	}

	/**
	 * 2. Display information
	 * 
	 * @param arrAsiaCountries
	 * @see AsiaCountry
	 */
	private void doDisplay(AsiaCountry[] arrAC) {
		if (arrAC != null)
			for (int i = 0; i < arrAC.length; i++)
				arrAC[i].display();
	}

	/**
	 * 1. Input information for n countries of Asia
	 * 
	 * @param numberInput
	 * @return array of AsiaCountry {@link}
	 * @see AsiaCountry
	 */

	private AsiaCountry[] doInput(int numberInput) {
		// TODO Auto-generated method stub
		AsiaCountry[] arrAC = new AsiaCountry[numberInput];
		for (int i = 0; i < numberInput; i++) {
			arrAC[i] = new AsiaCountry();
			arrAC[i].input();
		}
		return arrAC;
	}

	/**
	 * This method use to make fake data.
	 * 
	 * @deprecated Shouldn't use this method to insert data
	 * @param numberInput
	 *            number of entities want to input
	 * @return AsiaCountry[] array data of AsiaCountry <br/>
	 *         {@link}<br/>
	 *         <blockquote> <a href=http://google.com.vn">Google Link</a>}
	 */
	private AsiaCountry[] fakeData(int numberInput) {
		AsiaCountry[] arrAC = new AsiaCountry[numberInput];
		for (int i = 0, j = 2; i < arrAC.length; i++, j += 3)
			
			arrAC[i] = new AsiaCountry("coutryName" + j, 234d * j * 3,
					43l * j * 3, "location" + 7l * j * 3);
		
		return arrAC;
	}
}
