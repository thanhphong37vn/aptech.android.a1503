package com.aptech.android.hoan.country;

import java.util.SimpleTimeZone;

/**
 * 
 * @author Admin
 * @version v.05.23<br/>
 * {@link SimpleTimeZone}
 */
public class Constant {
	public static final String vn_VN = "vn_VN";
	public static final int MAX = 10;
	public static final int MIN = 0;
}
