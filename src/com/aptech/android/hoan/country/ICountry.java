package com.aptech.android.hoan.country;

/**
 * This base class
 * 
 * @author Admin
 * @version 20150-05-30
 */
public interface ICountry {

	/**
	 * Input country information
	 */
	public void input();

	/**
	 * Display country information
	 */
	public void display();

	/**
	 * A
	 * 
	 * @param area
	 * @param numberPerson
	 * @return arvage 
	 */
	public float everageArea(double area, long numberPerson);
}
