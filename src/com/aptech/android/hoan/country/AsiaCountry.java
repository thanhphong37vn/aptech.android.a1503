package com.aptech.android.hoan.country;

import java.util.Scanner;

import static com.aptech.android.hoan.country.Print.println;

/**
 * This method extend from Country class
 * 
 * @author Admin
 * 
 */
public class AsiaCountry extends CountryImpl {
	/** vi tri dia ly: Dong Nam A, Trung A hoac Nam A */
	private String location;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public AsiaCountry() {
		// TODO Auto-generated constructor stub
	}

	public AsiaCountry(String coutryName, double area, long numberPerson,
			String location) {
		super(coutryName, area, numberPerson);
		this.location = location;
	}

	public AsiaCountry(String location) {
		super();
		this.location = location;
	}

	/**
	 * This method to input country information
	 */
	public void input() {
		super.input();
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter("\n");
		println("Nhap vi tri dia ly");
		this.location = scanner.nextLine();
	}

	/**
	 * This method to display country information
	 */
	public void display() {
		println(this.toString());
	}

	@Override
	public String toString() {
		return super.toString().replace("]",
				", location= " + this.location + "]");
	}

}
