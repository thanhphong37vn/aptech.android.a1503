package com.aptech.android.hoan.country;

import java.util.Scanner;

import static com.aptech.android.hoan.country.Print.*;

/**
 * This class
 * 
 * @author Admin
 * 
 */
public class CountryImpl implements ICountry {

	/** Name of Country */
	private String coutryName;
	/** Area of Country */
	double area;
	/** Number person of Country */
	long numberPerson;

	/**
	 * This is constructor method non parameter to create instance Country
	 * object
	 */
	public CountryImpl() {
	}

	/**
	 * This is constructor method three parameter to create instance Country
	 * object
	 * 
	 * @param coutryName
	 * @param area
	 * @param numberPerson
	 */
	public CountryImpl(String coutryName, double area, long numberPerson) {
		super();
		this.coutryName = coutryName;
		this.area = area;
		this.numberPerson = numberPerson;
	}

	public String getCoutryName() {
		return coutryName;
	}

	public void setCoutryName(String coutryName) {
		this.coutryName = coutryName;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public long getNumberPerson() {
		return numberPerson;
	}

	public void setNumberPerson(long numberPerson) {
		this.numberPerson = numberPerson;
	}

	/**
	 * This method use to input country information
	 */
	public void input() {
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter("\n");
		println("=========Nhập thông tin thành phố=========\n");
		System.out.println("Nhập tên thành phố: ");
		this.coutryName = scanner.nextLine();
		println("Nhập diện tích : ");
		this.area = Double.parseDouble(scanner.nextLine());
		println("Nhập dân số : ");
		this.numberPerson = Long.parseLong(scanner.nextLine());
	}

	/**
	 * This method use to display country information
	 */
	public void display() {
		println(toString());
	}

	public String toString() {
		return "Country [coutryName=" + coutryName + ", area=" + area
				+ ", numberPerson=" + numberPerson + ", everageArea = "
				+ everageArea(area, numberPerson) + "]";
	}

	/**
	 * 
	 * Return area / numberPerson
	 * 
	 * @param area
	 *            ,
	 * @return
	 */
	public float everageArea(double area, long numberPerson) {
		return (float) (area / numberPerson);
	}
}
