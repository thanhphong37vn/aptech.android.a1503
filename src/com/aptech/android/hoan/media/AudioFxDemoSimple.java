/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aptech.android.hoan.media;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

public class AudioFxDemoSimple extends Activity {
	public static String mp3Url = "https://ia600506.us.archive.org/3/items/aesop_fables_volume_one_librivox/fables_01_00_aesop.mp3";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		TextView textView = new TextView(this);
		StringBuilder builder = new StringBuilder();

		builder.append("Tôi yêu Việt Nam\n\n\n");
		builder.append("MediaPlayer player = new MediaPlayer();\n");
		builder.append("player.setAudioStreamType(AudioManager.STREAM_MUSIC);\n");
		builder.append("player.setDataSource(\"https://ia600506.us.archive.org/3/items/aesop_fables_volume_one_librivox/fables_01_00_aesop.mp3\");\n"
				+ "player.prepare();\n");
		builder.append("player.start();");
		textView.setText(builder.toString());

		try {
			MediaPlayer player = new MediaPlayer();
			player.setAudioStreamType(AudioManager.STREAM_MUSIC);
			player.setDataSource(mp3Url);
			player.prepare();
			player.start();

		} catch (Exception e) {
			// TODO: handle exception
		}
		setContentView(textView);

	}
}