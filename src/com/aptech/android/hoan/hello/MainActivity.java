package com.aptech.android.hoan.hello;

import java.io.IOException;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

	public static String mp3Url = "https://ia600506.us.archive.org/3/items/aesop_fables_volume_one_librivox/fables_01_00_aesop.mp3";

	MediaPlayer mediaPlayer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mediaPlayer = new MediaPlayer();

		try {
			mediaPlayer.setDataSource(mp3Url);
			mediaPlayer.prepare();
			mediaPlayer.start();
		} catch (IOException e) {
			Log.v("AUDIOHTTPPLAYER", e.getMessage());
		}
//		setContentView(R.layout.activity_main);
	}

}
